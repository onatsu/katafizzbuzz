<?php

use factoriaF5\Kata;
use PHPUnit\Framework\TestCase;

class KataTest extends TestCase
{

  public function testReturnANumberIfNotHaveAnyCoincidence()
  {
    $kata = new Kata();
    $response = $kata->returnValue(1);

    $this->assertIsInt($response);
  }

  public function testReturnTheSameNumberIfNotHaveCoincidence(){
        $number = 4;
      $kata = new Kata();
      $response = $kata->returnValue($number);

      $this->assertEquals($number,$response);
  }

  public function testReturnFizzIfNumberIsDivisibleBy3(){
      $kata = new Kata();
      $response = $kata->returnValue(3);

      $this->assertEquals('Fizz',$response);
  }

  public function testResturnBuzzIfNumberIsDivisibleBy5(){
      $kata = new Kata();
      $response = $kata->returnValue(5);

      $this->assertEquals('Buzz',$response);
  }

}
