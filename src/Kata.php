<?php

namespace factoriaF5;

class Kata
{

    public function returnValue($number){
        $result = $number % 3;
        if($result == 0){
            return 'Fizz';
        }

        $result = $number % 5;
        if($result == 0){
            return 'Buzz';
        }

        return $number;
    }
}
